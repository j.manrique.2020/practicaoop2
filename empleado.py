
from persona import Persona

class Empleado(Persona):
    def __init__(self, n, s, nif=None):
        super().__init__(n,nif)
        self.nomina = s
        if s<0 :
            self.nomina=0
            
    def calculoimpuestos(self):
        return self.nomina*0.3

    def __str__(self):
        return "Los impuestos a pagar por {name} son {tax:.2f} euros".format(name=self.nombre,tax=self.calculoimpuestos())
