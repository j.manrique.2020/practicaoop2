
from jefe import Jefe
from empleado import Empleado
from persona import Persona
import unittest

class TestPersonas(unittest.TestCase):
    
    def test_construir_nombre_persona(self):
        p1=Persona("Pepe")
        self.assertEqual(p1.nombre,"Pepe")
    
    def test_construir_Nif_persona(self):
        p1=Persona("nombre","49897996Z")
        self.assertEqual(p1.nif,"49897996Z")
    
    def test_construir_Nif_none_persona(self):
        p1=Persona("nombre")
        self.assertEqual(p1.nif,None)

    def test_construir_empleado(self):
        e1=Empleado("nombre",5000)
        self.assertEqual(e1.nomina,5000)

    def test_impuestos_empleado(self):
        e1=Empleado("nombre",5000)
        self.assertEqual(e1.calculoimpuestos(),1500)

    def test_str_empleado(self):
        e1=Empleado("Pepe",5000)
        self.assertEqual("Los impuestos a pagar por Pepe son 1500.00 euros",e1.__str__())

    def test_nomina_negativa_empleado(self):
        e1=Empleado("nombre",-5000)
        self.assertEqual(e1.nomina,0)

    def test_construir_jefe(self):
        j1=Jefe("nombre",5000)
        self.assertEqual(j1.nomina,5000)

    def test_impuestos_jefe(self):
        j1=Jefe("nombre",5000,1000)
        self.assertEqual(j1.calculoimpuestos(),1800)

    def test_str_jefe(self):
        j1=Jefe("Ana",5000,1000)
        self.assertEqual("Los impuestos a pagar por el Jefe Ana son 1800.00 euros",j1.__str__())

    def test_nomina_negativa_jefe(self):
        j1=Jefe("nombre",-5000)
        self.assertEqual(j1.nomina,0)
    
if __name__=="__main__":
    unittest.main()
