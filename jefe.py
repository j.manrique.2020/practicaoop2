
from empleado import Empleado


class Jefe(Empleado):
    def __init__(self, n, s, extra=0, nif=None):
        super().__init__(n,s,nif)
        self.extra= extra

    def calculoimpuestos(self):
        return (self.nomina+self.extra)*0.3

    def __str__(self):
        return "Los impuestos a pagar por el Jefe {name} son {tax:.2f} euros".format(name=self.nombre,tax=self.calculoimpuestos())
